export const CounterComponent = {
  template: `
  <div>
    <p>State pada CounterComponent {{ counter }} </p>
  </div>
  `,
  computed: {
    counter() {
      // return this.$store.state.counter; /* menggunakan $store untuk memanggil state yang disimpan pada Vuex di file lain */
      return this.$store.getters.counter; /* menggunakan getters untuk mengambil state pada Vuex */
      // return 'test';
    },
  },
};
