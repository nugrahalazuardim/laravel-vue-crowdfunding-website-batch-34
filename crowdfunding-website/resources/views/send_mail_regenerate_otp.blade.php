<!DOCTYPE html>
<html>

<head>
  <meta http-equiv="Content-type" content="text/html; charset=UTF-8">
</head>

<body style="font-family: sans-serif;">
  <div style="display: block; margin: auto; max-width: 600px" class="main">
    <h2 style="font-size: 18px; font-weight: bold; margin-top: 20px; text-align: center;">
      Otp baru telah dibuat
    </h2>
    <p style="text-align: center;">Silahkan masukan otp berikut untuk melanjutkan</p>
    <div style="background-color: orange; color: white; text-align: center">
      <h4 style="padding: 2px"><strong>{{ $otp ?? ''}}</strong></h4>
    </div>
    <p>Gunakan kode otp ini sebelum 5 menit dari <strong>sekarang!!</strong></p>
  </div>
  <style>
    .main {
      background-color: white;
    }

    a:hover {
      border-left-width: 1em;
      min-height: 2em;
    }
  </style>
</body>

</html>