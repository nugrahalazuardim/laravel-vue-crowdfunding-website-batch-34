<?php

namespace App;

use App\Traits\UseUUID;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
  use UseUUID;

  protected $fillable = [
    'username', 'email', 'name', 'password', 'photo_profile', 'role_id'
  ];

  protected $hidden = [
    'password',
  ];

  protected $casts = [
    'email_verified_at' => 'datetime'
  ];

  protected $primaryKey = 'id';

  public static function boot()
  {
    parent::boot();

    static::creating(function ($model) {
      $model->role_id = $model->get_role_user();
    });

    static::created(function ($model) {
      $model->generate_otp_code();
    });
  }

  public function get_role_user()
  {
    $role = Role::where('nama', 'user')->first();

    return $role->id;
  }

  public function generate_otp_code()
  {
    do {
      $random = mt_rand(100000, 999999);
      $check = Otp_code::where('otp', $random)->first();
    } while ($check);

    $now = Carbon::now();

    $otp_code = Otp_code::updateOrCreate(
      ['user_id' => $this->id],
      [
        'otp' => $random,
        'valid_until' => $now->addMinutes(5),
      ]
    );
  }

  public function getJWTIdentifier()
  {
    return $this->getKey();
  }

  /**
   * Return a key value array, containing any custom claims to be added to the JWT.
   *
   * @return array
   */
  public function getJWTCustomClaims()
  {
    return [];
  }

  public function role()
  {
    return $this->belongsTo(Role::class, 'role_id');
  }

  public function otpCode()
  {
    return $this->hasOne(Otp_code::class, 'user_id', 'id');
  }
}
