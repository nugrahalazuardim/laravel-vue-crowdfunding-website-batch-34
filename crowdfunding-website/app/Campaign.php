<?php

namespace App;

use App\Traits\UseUUID;
use Illuminate\Database\Eloquent\Model;

class Campaign extends Model
{
  use UseUUID;

  protected $guarded = [];
}
