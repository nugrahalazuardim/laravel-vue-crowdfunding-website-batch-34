<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait UseUUID
{
  public function getIncrementing()
  {
    return false;
  }

  public function getKeyType()
  {
    return 'string';
  }

  protected static function bootUseUUID()
  {
    static::creating(function ($model) {
      if (empty($model->{$model->getKeyName()})) {
        $model->{$model->getKeyName()} = Str::uuid();
      }
    });
  }
}
