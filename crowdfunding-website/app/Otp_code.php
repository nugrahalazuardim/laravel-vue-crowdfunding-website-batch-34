<?php

namespace App;

use App\Traits\UseUUID;
use Illuminate\Database\Eloquent\Model;

class Otp_code extends Model
{
  use UseUUID;

  protected $fillable =  [
    'otp', 'user_id', 'valid_until'
  ];

  protected $primaryKey = 'id';

  public function user()
  {
    $this->belongsTo(User::class, 'user_id');
  }
}
