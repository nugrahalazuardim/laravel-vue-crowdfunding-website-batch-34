<?php

namespace App\Listeners;

use App\Events\UserRegisteredEvents;
use App\Mail\UserRegisteredMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailNotificationRegistered
{
  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Handle the event.
   *
   * @param  UserRegisteredEvents  $event
   * @return void
   */
  public function handle(UserRegisteredEvents $event)
  {
    Mail::to($event->user)->send(new UserRegisteredMail($event->user));
  }
}
