<?php

namespace App\Listeners;

use App\Events\RegenerateOtpEvents;
use App\Mail\RegenerateOtpMail;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailNotificationRegenerated
{
  /**
   * Create the event listener.
   *
   * @return void
   */
  public function __construct()
  {
    //
  }

  /**
   * Handle the event.
   *
   * @param  RegenerateOtpEvents  $event
   * @return void
   */
  public function handle(RegenerateOtpEvents $event)
  {
    Mail::to($event->user)->send(new RegenerateOtpMail($event->user));
  }
}
