<?php

namespace App\Mail;

use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class RegenerateOtpMail extends Mailable
{
  use Queueable, SerializesModels;

  protected $user;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct(User $user)
  {
    $this->user = $user;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this
      ->from('example@example.com')
      ->view('send_mail_regenerate_otp')
      ->with([
        'otp' => $this->user->otpCode->otp,
      ]);
  }
}
