<?php

namespace App;

use App\Traits\UseUUID;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
  use UseUUID;

  protected $fillable = ['nama'];
  protected $primaryKey = 'id';

  public function user()
  {
    return $this->hasMany(User::class, 'role_id');
  }
}
