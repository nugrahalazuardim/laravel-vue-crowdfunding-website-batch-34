<?php

namespace App\Http\Controllers\User;

use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
  public function index()
  {
    $data['user'] = auth()->user();

    return response()->json([
      'response_code' => '01',
      'response_message' => 'Data profil berhasil dibuka',
      'data' => $data
    ]);
  }

  public function update(Request $request)
  {
    $user = auth()->user();

    if ($request->hasFile('photo_profile')) {
      $photo_profile = $request->file('photo_profile');
      $photo_profile_extension = $photo_profile->getClientOriginalExtension();
      $photo_profile_name = Str::slug($user->name, '-') . '-' . $user->id . '.' . $photo_profile_extension;
      $photo_profile_folder = '/photo/users/photo-profile/';
      $photo_profile_location = $photo_profile_folder . $photo_profile_name;
      try {
        $photo_profile->move(public_path($photo_profile_folder), $photo_profile_name);
        $user->update([
          'photo_profile' => $photo_profile_location,
        ]);
      } catch (\Throwable $th) {
        return response()->json([
          'response_code' => '00',
          'response_message' => 'Foto profil gagal dirubah',
          'data' => $data,
        ], 400);
      }
    }

    $user->update([
      'name' => $request->name,
    ]);

    $data['user'] = $user;

    return response()->json([
      'response_code' => '01',
      'response_message' => 'Profil berhasil dirubah',
      'data' => $data,
    ]);
  }
}
