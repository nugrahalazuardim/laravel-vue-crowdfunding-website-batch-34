<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class LoginController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    $request->validate([
      'email' => 'required|email',
      'password' => 'required'
    ]);

    $credentials = request(['email', 'password']);

    if (!$token = auth()->attempt($credentials)) {
      return response()->json(['error' => 'Email atau Password salah'], 401);
    }

    return response()->json([
      'response_code' => '01',
      'response_message' => 'Pengguna berhasil melakukan login',
      'data' => [
        'user' => auth()->user(),
        'token' => $token,
      ]
    ], 200);
  }
}
