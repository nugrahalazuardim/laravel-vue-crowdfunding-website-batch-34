<?php

namespace App\Http\Controllers\Auth;

use App\Events\UserRegisteredEvents;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Mail\UserRegisteredMail;
use Illuminate\Support\Facades\Mail;


class RegisterController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    $request->validate([
      'email' => 'required|unique:users,email|email',
      'username' => 'required|unique:users,username',
      'name' => 'required',
    ]);

    $data_request = $request->all();
    $user = User::create($data_request);

    // testing
    // Mail::to($event->user)->send(new UserRegisteredMail($event->user));

    event(new UserRegisteredEvents($user));

    $data['user'] = $user;

    return response()->json([
      'response_code' => '01',
      'response_messsage' => 'Pengguna baru berhasil didaftarkan, silahkan cek email untuk kode otp',
      'data' => $data
    ]);
  }
}
