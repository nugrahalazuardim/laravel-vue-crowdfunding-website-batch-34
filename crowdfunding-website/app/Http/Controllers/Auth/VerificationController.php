<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Otp_code;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VerificationController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    $request->validate([
      'otp' => 'required',
    ]);

    $otp_code = Otp_code::where('otp', $request->otp)->first();

    if (!$otp_code) {
      return response()->json([
        'response_code' => '00',
        'response_message' => 'Otp code tidak ditemukan',
      ], 400);
    }

    $now = Carbon::now();

    if ($now > $otp_code->valid_until) {
      return response()->json([
        'response_code' => '00',
        'response_message' => 'Otp code sudah tidak berlaku, silahkan akses ulang'
      ], 400);
    }

    $user = User::find($otp_code->user_id);
    $user->email_verified_at = $now;
    $user->save();

    $otp_code->delete();

    $data['user'] = $user;
    return response()->json([
      'response_code' => '01',
      'response_message' => 'Pengguna berhasil di verifikasi',
      'data' => $data
    ], 200);
  }
}
