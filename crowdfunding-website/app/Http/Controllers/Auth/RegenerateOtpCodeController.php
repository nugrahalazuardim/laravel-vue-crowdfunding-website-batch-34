<?php

namespace App\Http\Controllers\Auth;

use App\Events\RegenerateOtpEvents;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RegenerateOtpCodeController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    $request->validate([
      'email' => 'required',
    ]);

    $user = User::where('email', $request->email)->first();

    $user->generate_otp_code();

    event(new RegenerateOtpEvents($user));

    $data['user'] = $user;

    return response()->json([
      'response_code' => '01',
      'response_message' => 'Kode otp berhasil di akses ulang',
      'data' => $data
    ]);
  }
}
