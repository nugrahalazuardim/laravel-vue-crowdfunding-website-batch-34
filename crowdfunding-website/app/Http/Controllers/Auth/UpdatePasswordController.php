<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UpdatePasswordController extends Controller
{
  /**
   * Handle the incoming request.
   *
   * @param  \Illuminate\Http\Request  $request
   * @return \Illuminate\Http\Response
   */
  public function __invoke(Request $request)
  {
    $request->validate([
      'email' => 'required|email',
      'password' => 'required|confirmed|min:6',
    ]);

    User::where('email', $request->email)->update([
      'password' => bcrypt($request->password),
    ]);

    return response()->json([
      'response_code' => '01',
      'response_message' => 'Kata sandi berhasil dirubah',
    ], 200);
  }
}
