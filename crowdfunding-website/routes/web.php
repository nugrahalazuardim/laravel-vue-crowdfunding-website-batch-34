<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//   return view('welcome');
// });

// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

// Route 1 dengan middleware menggunakan email verifikasi
// Route::get('/route-1', function () {
//   return "Masuk ke route 1 email sudah diverifikasi";
// })->middleware('auth', 'email_verified');

// Route 2 dengan middleware menggunakan role user yang telah melakukan register
// Route::get('/route-2', function () {
//   return "Masuk ke route 2 role telah diizinkan";
// })->middleware('auth', 'is_admin');

// Mail testing
// Route::get('/test', function () {
//   return view('send_mail_user_registered');
// });

Route::view('/{any?}', 'app')->where('any', '.*');
