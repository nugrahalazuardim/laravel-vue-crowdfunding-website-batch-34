const barisBaru = () => {
  console.log('');
};

/* Soal 1 */
/* 
  buatlah fungsi menggunakan arrow function luas dan keliling persegi
  panjang dengan arrow function lalu gunakan let atau const di dalam 
  soal ini
 */
const luasPersegi = (p, l) => {
  let luas = p * l;
  return `Luas persegi panjang dengan panjang ${p} dan lebar ${l} adalah ${luas}`;
};

const kelilingPersegi = (p, l) => {
  let keliling = 2 * (p + l);
  return `Keliling persegi panjang dengan panjang ${p} dan lebar ${l} adalah ${keliling}`;
};

console.log('Soal 1');
console.log(luasPersegi(3, 4));
console.log(kelilingPersegi(3, 4));
barisBaru();
/* Jawaban soal 1 */

/* Soal 2 */
/* 
  Ubahlah code di bawah ini ke dalam arrow function dan object literal ES6 
  yang lebih sederhana
  const newFunction = function literal(firstName, lastName) {
      firstName: firstName,
      lastName: lastName,
      fullName: function() {
        console.log(firstName + " " + lastName)
      }
    }
  }
  */
const newFunction = (literal = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(`${firstName} ${lastName}`);
    },
  };
});

console.log('Soal 2');
newFunction('William', 'Imoh').fullName();
barisBaru();
/* Jawaban soal 2 */

/* Soal 3 */
/* 
  Diberikan sebuah objek sebagai berikut,
  Gunakan metode destructuring dalam ES6 untuk mendapatkan semua
  nilai dalam object dengan lebih singkat (1 line saja)
  */
const newObject = {
  firstName: 'Muhammad',
  lastName: 'Iqbal Mubarok',
  address: 'Jalan Ranamanyar',
  hobby: 'playing football',
};

const { firstName, lastName, address, hobby } = newObject;

console.log('Soal 3');
console.log(firstName, lastName, address, hobby);
barisBaru();
/* Jawaban soal 3 */

/* Soal 4 */
/* 
  Kombinasikan dua array berikut menggunakan array spreading ES6
  */
const west = ['Will', 'Chris', 'Sam', 'Holly'];
const east = ['Gill', 'Brian', 'Noel', 'Maggie'];
let combined = [...west, ...east];

console.log('Soal 4');
console.log(combined);
barisBaru();
/* Jawaban soal 4 */

/* Soal 5 */
/* 
  Sederhanakan string berikut agar menjadi lebih sederhana menggunakan template
  literal ES6
  */
const planet = 'earth';
const view = 'glass';
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet}`;

console.log('Soal 5');
console.log(before);
barisBaru();
/* Jawaban soal 5 */
