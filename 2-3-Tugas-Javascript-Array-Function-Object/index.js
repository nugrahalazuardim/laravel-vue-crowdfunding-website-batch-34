function barisBaru() {
  console.log('');
}

// Soal 1
var daftarHewan = ['2. Komodo', '5. Buaya', '3. Cicak', '4. Ular', '1. Tokek'];

daftarHewan.sort();
console.log('Soal 1');
console.log(daftarHewan);

for (j = 0; j < daftarHewan.length; j++) {
  console.log(daftarHewan[j]);
}
barisBaru();

// Soal 2
function introduce(obj) {
  return `Nama saya ${obj.name}, umur saya ${obj.age} tahun, alamat saya di ${obj.address}, dan saya punya hobby yaitu ${obj.hobby}`;
}

var data = {
  name: 'John',
  age: 30,
  address: 'Jalan Pelesiran',
  hobby: 'Gaming',
};

var perkenalan = introduce(data);
console.log('Soal 2');
console.log(perkenalan);
barisBaru();

// Soal 3
var vokal = ['a', 'i', 'u', 'e', 'o'];

function hitung_huruf_vokal(str) {
  var hitung = 0;

  for (var kata of str.toLowerCase()) {
    if (vokal.includes(kata)) {
      hitung++;
    }
  }

  return hitung;
}

var hitung_1 = hitung_huruf_vokal('Muhammad');
var hitung_2 = hitung_huruf_vokal('Iqbal');

console.log('Soal 3');
console.log(hitung_1, hitung_2);
barisBaru();

// Soal 4
function hitung(int) {
  return 2 * int - 2;
}

console.log('Soal 4');
console.log(hitung(0));
console.log(hitung(1));
console.log(hitung(2));
console.log(hitung(3));
console.log(hitung(5));
barisBaru();
