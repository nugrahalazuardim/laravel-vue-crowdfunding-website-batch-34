# Laravel Vue Crowdfunding Website Batch 34

Latihan dan Proyek Bootcamp Laravel Vue Sanbercode

<p align="center">
<a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a>
<a href="https://vuejs.org" target="_blank" rel="noopener noreferrer"><img width="100" src="https://vuejs.org/images/logo.png" alt="Vue logo"></a>
</p>

## Tentang Repo

Repo ini dibuat sebagai dokumentasi pembelajaran mengenai Laravel Vue dalam pembuatan website crowdfunding pada bootcamp Sanbercode.

## Daftar Tugas

Berikut informasi umum tugas harian dan proyek bootcamp **Laravel Vue Crowdfunding Website**

| Nama Tugas | Judul                                                                                                                                                               |
| ---------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Tugas 1    | [Tugas pendalaman oop php](https://gitlab.com/nugrahalazuardim/laravel-vue-crowdfunding-website-batch-34/-/commit/f06286e7e6a6be9b6cf85cdd02798c7bd3766f15)         |
| Tugas 2    | [Tugas pengenalan uuid](https://gitlab.com/nugrahalazuardim/laravel-vue-crowdfunding-website-batch-34/-/commit/bb7598350ad77c941f2427b31224b34dccdd7b62)            |
| Tugas 3    | [Tugas laravel middleware](https://gitlab.com/nugrahalazuardim/laravel-vue-crowdfunding-website-batch-34/-/commit/db56a8ee8586edf2161a79854e1a4e1f91b4a8c2)         |
| Tugas 4    | [Tugas Web Service](https://gitlab.com/nugrahalazuardim/laravel-vue-crowdfunding-website-batch-34/-/commit/ebb4d72069a415a3dd77be0aacb8c3cc4b7c938b)                |
| Tugas 5    | [Tugas JWT Auth](https://gitlab.com/nugrahalazuardim/laravel-vue-crowdfunding-website-batch-34/-/commit/7c538736e125c60f8fb81e187e5bf167da972747)                   |
| Tugas 6    | [Tugas Mail, Listener and Queue](https://gitlab.com/nugrahalazuardim/laravel-vue-crowdfunding-website-batch-34/-/commit/c050b36eb14c9b2a41e174e5cd73bf07e5b675b7)   |
| Tugas 7    | [Tugas Array, Function and Object](https://gitlab.com/nugrahalazuardim/laravel-vue-crowdfunding-website-batch-34/-/commit/db2b805568db407b7ddd75dedff15f1b44ca1449) |
| Tugas 8    | [Tugas ES6](https://gitlab.com/nugrahalazuardim/laravel-vue-crowdfunding-website-batch-34/-/commit/f6e785c6836478bc024cfaa01ef18e03dedf3413)                        |
| Tugas 9    | [Tugas Pengenalan Vue JS](https://gitlab.com/nugrahalazuardim/laravel-vue-crowdfunding-website-batch-34/-/commit/04b8aeef93e5b639e61c4acb147918e73e7a9cbf)          |
| Tugas 10   | [Tugas Form Input Binding AJAX](https://gitlab.com/nugrahalazuardim/laravel-vue-crowdfunding-website-batch-34/-/commit/7c23f50301a85e5450dc368b0088eff050866227)    |
| Tugas 11   | [Tugas Vue Component](https://gitlab.com/nugrahalazuardim/laravel-vue-crowdfunding-website-batch-34/-/commit/ae61891597d78eae86019e356b5bff861c47b690)              |

## Dokumentasi Tugas Akhir

1. SCREENSHOT
<p>
  <img src="/uploads/7ff0a000c6cf7592bd23c09ec606ba5a/Dashboard.png" width="580">
  <img src="/uploads/2031d79d04a82cbcb8cb0cc5dd4cb284/Dashboard-2.png" width="580">
</p>

<p>
  <img src="/uploads/c1699b1c8644bca02b0992ad59c8bfff/Campaign-detail.png" width="580">
  <img src="/uploads/e3fce3bbd8bcbfe4ec671908b875c5a4/Transaction-increase.png" width="580">
</p>

<p>
  <img src="/uploads/3d80d0aad5a51941d0cea6a7f0b57a3a/Searching-campaign.png" width="580">
  <img src="/uploads/9c4e22247e0915962e0cf151fd7e5fad/Search-not-found.png" width="580">
</p>

<p>
  <img src="/uploads/9d8b83ae878c9aa01bbb80dd36cb3561/Login-with-local-account.png" width="580">
  <img src="/uploads/5877813dfa14dcb3b6edecd73fec5f53/Login-with-social-media-account.png" width="580">
</p>

<p>
  <img src="/uploads/80d1cab441b01398416f4493ea290c5e/Local-account-login.png" width="580">
  <img src="/uploads/2336cf611ad20d5108b70b5c7bc3a368/Social-media-account-login.png" width="580">
</p>

<p>
  <img src="/uploads/d69cf1f4c05418ce1082a27dfc14f818/Logout-success.png" width="580">
</p>

2. APPLIACTION DEMO

**[Link demo aplikasi](https://drive.google.com/file/d/1QToSKRwawDGLw5O0OATazPdNZETw0q7v/view?usp=sharing)**

> untuk melakukan editing markdown yang ada, baca **[dokumentasi](https://daringfireball.net/projects/markdown/syntax)** resminya
