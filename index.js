// var sayHello = 'Hello World!';
// console.log(sayHello);

// var hobbies = ['coding', 'cycling', 'climbing', 'skateboarding'];
// console.log(hobbies);
// console.log(hobbies.length);
// console.log(hobbies[0]);
// console.log(hobbies[2]);
// console.log(hobbies[hobbies.length - 1]);

// var feeling = ['dag', 'dig'];
// feeling.push('dug');
// feeling.pop();
// console.log(feeling);

// var numbers = [0, 1, 2];
// numbers.push(3);
// console.log(numbers);
// numbers.push(4, 5);
// console.log(numbers);

// var numbers = [0, 1, 2, 3, 4, 5];
// numbers.pop();
// console.log(numbers);

// var numbers = [0, 1, 2, 3];
// numbers.unshift(-1);
// console.log(numbers);
// numbers.shift();
// console.log(numbers);

// var animals = ['kera', 'gajah', 'musang'];
// animals.sort();
// console.log(animals);

// var angka = [0, 1, 2, 3];
// var irisan1 = angka.slice(1, 3);
// console.log(irisan1);
// var irisan2 = angka.slice(0, 2);
// console.log(irisan2);
// var irisan3 = angka.slice(1);
// console.log(irisan3);

// var fruits = ['banana', 'orange', 'grape'];
// fruits.splice(1, 0, 'watermelon');
// console.log(fruits);
// fruits.splice(0, 2);
// console.log(fruits);

var mobil = [
  {
    merk: 'BMW',
    warna: 'merah',
    tipe: 'sedan',
  },
  {
    merk: 'Toyota',
    warna: 'hitam',
    tipe: 'box',
  },
  {
    merk: 'Audi',
    warna: 'biru',
    tipe: 'sedan',
  },
];
// mobil.forEach(function (item) {
//   console.log('warna: ' + item.warna);
// });

// var arrayWarna = mobil.map(function (item) {
//   return item.warna;
// });

// console.log(arrayWarna);

var arrayMobilFilter = mobil.filter(function (item) {
  return item.tipe != 'sedan';
});

console.log(arrayMobilFilter);
