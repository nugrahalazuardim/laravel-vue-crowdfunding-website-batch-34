<?php

trait Hewan
{
  public $nama;
  public $darah = 50;
  public $jumlahKaki;
  public $keahlian;

  public function atraksi()
  {
    echo "{$this->nama} sedang {$this->keahlian}";
  }
}

abstract class Fight
{
  use Hewan;
  public $attackPower;
  public $defencePower;

  public function serang($hewan)
  {
    echo "{$this->nama} sedang menyerang {$hewan->nama} <br>";

    $hewan->diserang($this);
  }

  public function diserang($hewan)
  {
    echo "{$this->nama} sedang diserang {$hewan->nama}";

    $this->darah = $this->darah - $hewan->attackPower / $this->defencePower;
  }

  public function getInfo()
  {
    echo "<br>";
    echo "Nama: ($this->nama) <br>";
    echo "Jumlah kaki: ($this->jumlahKaki) <br>";
    echo "Keahlian: ($this->keahlian) <br>";

    echo "Darah: ($this->darah) <br>";
    echo "AP: ($this->attackPower) <br>";
    echo "DP: ($this->defencePower) <br>";
    $this->atraksi();
  }
  abstract public function getInfoHewan();
}

class Elang extends Fight
{
  public function __construct($nama)
  {
    $this->nama = $nama;
    $this->jumlahKaki = "2";
    $this->keahlian = "terbang tinggi";
    $this->attackPower = 10;
    $this->defencePower = 5;
  }

  public function getInfoHewan()
  {
    echo "Jenis hewan: Unggas berkaki dua";
    $this->getInfo();
  }
}

class Harimau extends Fight
{
  public function __construct($nama)
  {
    $this->nama = $nama;
    $this->jumlahKaki = "4";
    $this->keahlian = "lari cepat";
    $this->attackPower = 7;
    $this->defencePower = 8;
  }

  public function getInfoHewan()
  {
    echo "Jenis hewan: Mamalia berkaki empat";
    $this->getInfo();
  }
}

$elang = new Elang("Elang");
$elang->getInfoHewan();
echo "<br>";
echo "======= <br>";

$harimau = new Harimau("Harimau");
$harimau->getInfoHewan();
echo "<br>";
echo "======= <br>";

$elang->serang($harimau);

echo "<br>";
echo "======= <br>";
$harimau->getInfoHewan();
echo "<br>";
echo "======= <br>";
$elang->getInfoHewan();
